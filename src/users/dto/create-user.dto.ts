import { IsEmail, IsNotEmpty, Length } from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(8, 16)
  password: string;

  @IsNotEmpty()
  @Length(5, 20)
  fullName: string;

  // @IsArray()
  // @ArrayNotEmpty()
  // roles: ('admin' | 'user')[];
  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
